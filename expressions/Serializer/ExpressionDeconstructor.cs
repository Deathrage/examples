﻿using System.Linq.Expressions;
using System.Reflection;

namespace Serializer;

public class ExpressionDeconstructor: IDeconstructor
{
    public IDictionary<string, object?> Deconstruct(object data)
    {
        if (!_cache.TryGetValue(data.GetType(), out DeconstructDelegate? deconstruct))
        {
            Type dataType = data.GetType();

            deconstruct = CreateDeconstructDelegate(dataType);

            _cache.Add(dataType, deconstruct);
        }

        return deconstruct(data);
    }

    private readonly Dictionary<Type, DeconstructDelegate> _cache = new();

    private static readonly MethodInfo _dictionaryAddMethod =
        typeof(Dictionary<string, object?>).GetMethod(nameof(Dictionary<string, object?>.Add),
            BindingFlags.Instance | BindingFlags.Public) ?? throw new NullReferenceException();

    private delegate IDictionary<string, object?> DeconstructDelegate(object data);

    private static DeconstructDelegate CreateDeconstructDelegate(Type dataType)
    {
        PropertyInfo[] properties = dataType
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.CanRead)
                .ToArray();
        FieldInfo[] fields = dataType
            .GetFields(BindingFlags.Public | BindingFlags.Instance)
            .ToArray();
        MemberInfo[] members = properties
            .Cast<MemberInfo>()
            .Concat(fields)
            .ToArray();

        ParameterExpression dataParameter = Expression.Parameter(dataType, "data");

        // Dictionary<string, object?> propertiesAndFields;
        ParameterExpression dictionaryVariable = Expression.Parameter(typeof(Dictionary<string, object?>), "propertiesAndFields");
        // new Dictionary<string, object?>();
        NewExpression dictionaryNew = Expression.New(typeof(Dictionary<string, object?>));
        // Dictionary<string, object?> propertiesAndFields = new Dictionary<string, object?>();
        Expression dictionaryAssign = Expression.Assign(dictionaryVariable, dictionaryNew);

        // For every member (field or property) generates a row that looks like: dict.Add("member name", data.member);
        List<MethodCallExpression> addMethodCalls = new();
        foreach (MemberInfo member in members)
        {
            // data.member
            MemberExpression dataMemberAccess = Expression.MakeMemberAccess(dataParameter, member);
            // propertiesAndFields.Add("member name", (object)data.member);
            MethodCallExpression addCall = Expression.Call(dictionaryVariable, _dictionaryAddMethod, Expression.Constant(member.Name), Expression.Convert(dataMemberAccess, typeof(object)));

            addMethodCalls.Add(addCall);
        }

        /**
         * Dictionary<string, object?> propertiesAndFields = new Dictionary<string, object?>();
         * propertiesAndFields.Add("member 1", (object)data.member1);
         * propertiesAndFields.Add("member 2", (object)data.member2);
         * ...
         * return propertiesAndFields;
         */
        BlockExpression codeBlock = Expression.Block(
            typeof(IDictionary<string, object?>),
            // Variables of the block
            new ParameterExpression[] { dictionaryVariable },
            // Body of the block
            new Expression[] { dictionaryAssign }
                .Concat(addMethodCalls)
                // Return value is last
                .Append(dictionaryVariable));

        /**
         * (TypDataAtTobyloCokoliv data) => {
         *   Dictionary<string, object?> propertiesAndFields = new Dictionary<string, object?>();
         *   propertiesAndFields.Add("member 1", (object)data.member1);
         *   propertiesAndFields.Add("member 2", (object)data.member2);
         *   ...
         *   return propertiesAndFields;
         * }
         */
        LambdaExpression innerLambda = Expression.Lambda(codeBlock, dataParameter);

        ParameterExpression objectDataParameter = Expression.Parameter(typeof(object), "objectData");

        /**
         * (object objectData) => innerLambda((TypDataAtTobyloCokoliv)data);
         */
        Expression<DeconstructDelegate> lambda =Expression.Lambda<DeconstructDelegate>(
            Expression.Invoke(innerLambda, Expression.Convert(objectDataParameter, dataType)),
            objectDataParameter);

        return lambda.Compile();
    }
}