﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using Serializer;

// Ten milion times
int times = 10000000;

TestDto testDto = new(1, "Petr", "Novák");

RunMultipleTimes(new ReflectionDeconstructor(), testDto, times);
RunMultipleTimes(new ExpressionDeconstructor(), testDto, times);

void RunMultipleTimes(IDeconstructor deconstructor, object data, int times)
{
    Console.WriteLine($"Running {deconstructor.GetType().Name}.");

    Stopwatch stopwatch = Stopwatch.StartNew();

    for (int i = 0; i < times; i++)
    {
        deconstructor.Deconstruct(data);

        if (i == 0)
            Console.WriteLine($"First run took: {stopwatch.ElapsedMilliseconds}ms.");
    }

    stopwatch.Stop();

    Console.WriteLine($"Did {times} runs in {stopwatch.Elapsed}.");
}