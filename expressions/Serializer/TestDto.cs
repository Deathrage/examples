﻿namespace Serializer;

public class TestDto
{
    public int Id { get; }

    public string Name { get; }

    public string Description { get; }

    public TestDto(int id, string name, string description)
    {
        Id = id;
        Name = name ?? throw new ArgumentNullException(nameof(name));
        Description = description ?? throw new ArgumentNullException(nameof(description));
    }
}