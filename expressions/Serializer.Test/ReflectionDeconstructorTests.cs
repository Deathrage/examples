﻿namespace Serializer.Test;

public class ReflectionDeconstructorTests
{
    class Dto
    {
        public int Id { get; }

        public string Name { get; }

        public Dto(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }

    [Test]
    public void Deconstruct()
    {
        // Setup
        Dto dto = new(1, "Petr");

        ReflectionDeconstructor sut = new();

        // Act
        IDictionary<string, object?> result = sut.Deconstruct(dto);

        // Assert
        Assert.That(result, Has.Count.EqualTo(2));
        Assert.That(result, Contains.Key(nameof(Dto.Id)).WithValue(1));
        Assert.That(result, Contains.Key(nameof(Dto.Name)).WithValue("Petr"));
    }
}