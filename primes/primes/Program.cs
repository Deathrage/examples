﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace primes
{
    public class Program
    {
        /// <summary>
        /// This algo is called Sieve of Eratothenes (https://cs.wikipedia.org/wiki/Eratosthenovo_s%C3%ADto)
        /// Basically this works by creating table or array which is iterated through in squares
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static IEnumerable<int> GetAllPrimes(int limit)
        {
            // Create Array of length (n+1) where all elements are true
            // We will get indexes [0..limit]
            // We will pretend that all numbers in the range [0..limit] are prime
            bool[] primeNumbers = Enumerable.Repeat(true, limit + 1).ToArray();

            // The next step is to go through [0..limit] and remove (mark as false) all numbers that are not prime

            // We will go through all number between 0 and root of limit (root of limit is here to make algo faster, it's basically the same reason why we start from i*i in the second for cycle and not from i)
            // Why like this and not Math.Sqrt(limit)? because I want to use integer and not floats
            for (int i = 2; i * i <= limit; i++)
            {
                // If we meet a prime number (which has true on this index) we will start removing it's increments
                // We are starting from 2 and we know that 2 is prime number, thus in first iteration we will start removing increments of 2
                if (primeNumbers[i])
                {
                    // We are removing it's increments, we can start from i*i because numbers lower then i*i were taken care of in previous iterations (this makes the algo faster)
                    // i.e. for number 3 we can start from 9 as 3*2 was taken care of during removal of 2 increments and 3*1 is still 3
                    // i.e. for number 4, we won't even get here as 4 was removed during increments of 2 (in 2*2) thus the condition above will be false
                    // i.e. for number 5, we can start from 25 as 5*2 was taken care of during removal of 2 increments, 5*3 was taken care of during removal of 3 increments, 5*4 can be ignored (as we determined in increments of 2 that 4 isn't prime number)
                    // i.e. for number 6, we won't event get here as 6 was removed during increments of 2 (in 2*3) and thus condition above will be false (if we started increments of 3 from 3 and not from 3*3, the 6 would get removed again during increments of 3, in 3*2)
                    // i.e. for number 7, we can start from 49 as 7*2 was taken care of during 2 increments, 7*3 was taken care of during 3 increments, 7*4 can be ignored, 7*5 was taken care of in 5 increments, 7*6 can be ignored
                    for (int j = i * i; j <= limit; j += i)
                    {
                        primeNumbers[j] = false;
                    }
                }
            }

            // Now we have to find our prime numbers, if the index of array holds true, it must be prime number
            // I am using Pairs instead of Tuples as I might confuse you with their syntax (using C#'s LINQ - Language Integrated Query - might be confusing enough).
            IEnumerable<int> result = primeNumbers
                // as we no longer want to use Array indexes as numbers, transform it to Pair object
                .Select((isPrime, index) => new KeyValuePair<int, bool>(index, isPrime))
                // The fist number in [0..limit] is 0, which is not prime number so skip by one (skip the first)
                // If we did this before the transformation we would just shift everything one index back (we would shift bool value on index 1 to index 0)
                .Skip(1)
                // We stored the bool (representing whether the index/number is prime) in Value of our Pair, thus filter out all Paris that have Value as false (meaning they are not prime)
                .Where(pair => pair.Value)
                // Finally we stored our number (which was previously represented by array index) in key of our Pair, now we can finally disassemble the Pair and return the number
                .Select(pair => pair.Key);

            // Equivalent for the LINQ above would be
            /*
             List<int> result = new List<int>();
              for (int i = 2; i <= limit; i++) {
                 if (primeNumbers[i]) {
                     result.Add(i);
                 }
              }
              return result;
            */
            return result;
        }

        public static int AskForNumber()
        {
            int? number;
            while (true)
            {
                Console.WriteLine("Napiš celé číslo");
                string input = Console.ReadLine();
                if (int.TryParse(input, out int inputNumber))
                {
                    number = inputNumber;
                    break;
                }

                Console.WriteLine($"Hodnota {input} není celé číslo");
            }

            return number.Value;
        }

        public static void Main(string[] args)
        {
            int inputNumber = AskForNumber();

            IEnumerable<int> primes = GetAllPrimes(inputNumber);

            string result = string.Join(",", primes);

            Console.WriteLine(result);

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey(true);
        }
    }
}
