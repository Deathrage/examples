using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using primes;

namespace primtes.tests
{
    public class Tests
    {
        private static readonly object[] _source = new object[]
        {
            5,
            10,
            500,
            1,
            8,
            64,
            17
        };

        [Test]
        [TestCaseSource(nameof(_source))]
        public void Program_Main(int limit)
        {
            IEnumerable<int> foundPrimes = Program.GetAllPrimes(limit);

            IEnumerable<int> pregeneratedPrimes = KnownPrimesFixture.Primes.Where(num => num <= limit);

            CollectionAssert.AreEqual(pregeneratedPrimes, foundPrimes);
        }
    }
}