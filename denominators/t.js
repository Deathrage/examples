const mFunc = require('./func');
const adrianaFunc = require('./adriana-func');


const args = [4, 6, 5, 3];

let start = new Date();
for (let i = 0; i < 100000000; ++i) {
  mFunc(...args);
}

console.log('My func', new Date() - start);

start = new Date();
for (let i = 0; i < 100000000; ++i) {
  adrianaFunc(...args);
}

console.log('Adriana func', new Date() - start);