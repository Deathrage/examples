const findHighestDivisor = (a, b) => {
  if (!b)
    return a;
  return findHighestDivisor(b, a % b);
}

module.exports = (a, b, c, d) => {
  const numerator = a * d + c * b;
  const denominator = b * d;

  const highestDivisor = findHighestDivisor(numerator, denominator);

  return [numerator / highestDivisor, denominator / highestDivisor];
};