const sut = require('./func');

describe('test', () => {
  it('1', () => {
    expect(sut(4, 5, 3, 2)).toEqual([23, 10]);
  });
  it('2', () => {
    expect(sut(10, 2, 8, 4)).toEqual([7, 1]);
  });
  it('2', () => {
    expect(sut(4, 6, 5, 3)).toEqual([7, 3]);
  });
});