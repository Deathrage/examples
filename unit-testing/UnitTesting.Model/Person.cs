﻿namespace UnitTesting.Model;

public class Person
{
    public int Id { get; }

    public string Name { get; }

    public int Age { get; }

    public Person(int id, string name, int age)
    {
        Id = id;
        Name = name;
        Age = age;
    }
}

