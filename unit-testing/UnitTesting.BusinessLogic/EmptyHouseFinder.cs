﻿using UnitTesting.Model;

namespace UnitTesting.BusinessLogic;

public class EmptyHouseFinder
{
    public IEnumerable<House> Find(Town town)
    {
        foreach (Street street in town.Streets)
            foreach (House house in street.Houses)
                if (house.Occupants.Count == 0)
                    yield return house;
    }
}