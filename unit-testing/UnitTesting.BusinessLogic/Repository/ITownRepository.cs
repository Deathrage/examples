﻿using UnitTesting.Model;

namespace UnitTesting.BusinessLogic.Repository;

public interface ITownRepository
{
    Town Get(string name);

    IEnumerable<Town> GetAll();
}