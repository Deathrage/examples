﻿using UnitTesting.Model;

namespace UnitTesting.BusinessLogic.Repository;

public interface IPersonRepository
{
    Person Get(int id);
    
    IEnumerable<Person> GetAll();
}