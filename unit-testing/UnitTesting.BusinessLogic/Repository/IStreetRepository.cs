﻿using UnitTesting.Model;

namespace UnitTesting.BusinessLogic.Repository;

public interface IStreetRepository
{
    Street Get(string townName, string name);

    IEnumerable<Street> GetAll();
}