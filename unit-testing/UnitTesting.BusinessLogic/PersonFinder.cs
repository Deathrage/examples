﻿using UnitTesting.Model;

namespace UnitTesting.BusinessLogic;

public class PersonFinder
{
    public IEnumerable<Person> FindAdults(Town town)
    {
        foreach (Street street in town.Streets)
            foreach (House house in street.Houses)
                foreach (Person person in house.Occupants)
                    if (person.Age >= 18)
                        yield return person;
    }

    public IEnumerable<Person> FindUnaccompaniedChildren(Town town)
    {
        foreach (Street street in town.Streets)
            foreach (House house in street.Houses)
            {
                bool containsNoAdults = house.Occupants.Count > 0 && house.Occupants.All(occupant => occupant.Age < 18);
                if (containsNoAdults)
                    foreach (Person occupants in house.Occupants)
                        yield return occupants;
            }
    }
}