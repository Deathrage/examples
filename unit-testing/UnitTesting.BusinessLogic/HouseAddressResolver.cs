﻿using UnitTesting.BusinessLogic.Repository;
using UnitTesting.Model;

namespace UnitTesting.BusinessLogic;

public class HouseAddressResolver
{
    public HouseAddressResolver(IStreetRepository streetRepository, ITownRepository townRepository)
    {
        this.streetRepository = streetRepository;
        this.townRepository = townRepository;
    }

    public string? Resolve(House house)
    {
        Street[] streets = streetRepository
            .GetAll()
            .Where(street => street.Houses.Contains(house))
            .Take(2)
            .ToArray();

        if (streets.Length == 2)
            throw new InvalidOperationException("Multiple streets found for this house!");

        if (streets.Length == 0)
            return null;

        Street street = streets[0];

        Town[] towns = townRepository
            .GetAll()
            .Where(town => town.Streets.Contains(street))
            .Take(2)
            .ToArray();

        if (towns.Length == 2)
            throw new InvalidOperationException("Multiple streets found for this house!");

        if (towns.Length == 0)
            return null;

        Town town = towns[0];

        return $"{street.Name} {house.StreetNumber}, {town.Name}";
    }

    private readonly IStreetRepository streetRepository;
    private readonly ITownRepository townRepository;
}