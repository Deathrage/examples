﻿using UnitTesting.BusinessLogic.Repository;
using UnitTesting.Model;

namespace UnitTesting.CommandLine;

public class StaticDataRepository: ITownRepository, IStreetRepository, IHouseRepository, IPersonRepository
{
    public StaticDataRepository(Town[] towns)
    {
        _towns = towns;
        _persons = _towns.SelectMany(town =>
            town.Streets.SelectMany(street =>
                street.Houses.SelectMany(house => 
                    house.Occupants))).ToArray();
    }

    public Town Get(string name)
        => _towns.Single(town => town.Name == name);

    public Street Get(string townName, string name)
        => Get(townName).Streets.Single(street => street.Name == name);

    public House Get(string townName, string streeetName, string streetNumber)
        => Get(townName).Streets.Single(street => street.Name == streeetName).Houses.Single(house => house.StreetNumber == streeetName);

    public Person Get(int id)
        => _persons.Single(person => person.Id == id);

    IEnumerable<House> IHouseRepository.GetAll()
        => _towns.SelectMany(town => town.Streets.SelectMany(street => street.Houses));

    IEnumerable<Street> IStreetRepository.GetAll()
        => _towns.SelectMany(town => town.Streets);

    IEnumerable<Town> ITownRepository.GetAll()
        => _towns;

    public IEnumerable<Person> GetAll()
        => _persons;

    private readonly Person[] _persons;
    private readonly Town[] _towns;
}