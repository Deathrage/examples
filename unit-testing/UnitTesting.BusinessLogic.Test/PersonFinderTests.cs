﻿using NUnit.Framework;
using UnitTesting.Model;

namespace UnitTesting.BusinessLogic.Test;

public class PersonFinderTests
{
    [Test]
    public void FindAdults()
    {
        // Setup
        Person kid1 = new(1, "Dítě Malé", 10);
        Person kid2 = new(2, "Dítě Střední", 15);
        Person kid3 = new(3, "Dítě Velké", 17);
        Person adult1 = new(4, "John Doe", 50);
        Person adult2 = new(4, "Jane Doe", 20);
        House house1 = new("10/80", [kid1, kid2]);
        House house2 = new("10/80", [kid3, adult1, adult2]);
        Street street1 = new("bez dospěláků", [house1]);
        Street street2 = new("s dospěláky", [house2]);
        Town town = new("Testín", [street1, street2]);

        PersonFinder sut = new();

        // Act
        Person[] result = sut.FindAdults(town).ToArray();

        // Assert
        Assert.That(result, Is.EquivalentTo(new[] { adult1, adult2 }));
    }

    [Test]
    public void FindUnaccompaniedChildren()
    {
        // Setup
        Person kid1 = new(1, "Dítě Malé", 10);
        Person kid2 = new(2, "Dítě Střední", 15);
        Person kid3 = new(3, "Dítě Velké", 18);
        Person adult1 = new(4, "John Doe", 50);
        Person adult2 = new(4, "Jane Doe", 20);
        House house1 = new("10/80", [kid1, kid2]);
        House house2 = new("10/80", [kid3, adult1, adult2]);
        Street street1 = new("bez dospěláků", [house1]);
        Street street2 = new("s dospěláky", [house2]);
        Town town = new("Testín", [street1, street2]);

        PersonFinder sut = new();

        // Act
        Person[] result = sut.FindUnaccompaniedChildren(town).ToArray();

        // Assert
        Assert.That(result, Is.EquivalentTo(new[] { kid1, kid2 }));
    }
}